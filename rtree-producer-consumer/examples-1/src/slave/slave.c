/*
 * MIT License
 *
 * Copyright (c) 2011-2018 Pedro Henrique Penna <pedrohenriquepenna@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.  THE SOFTWARE IS PROVIDED
 * "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <nanvix/syscalls.h>
#include <nanvix/limits.h>
#include <nanvix/pm.h>
#include "index.h"
/**
 * @brief HAL RMem Microbenchmark Driver
 */



int MySearchCallback(int id, void* arg)
{
	// Note: -1 to make up for the +1 when data was inserted
        ((void) arg);
        printf("Hit data rect %d\n", id-1);
	return 1; // keep going
}


int main2(int argc, const char **argv)
{
	int nodenum;
	int barrier;
	int nclusters;
	int masternode;
	int nodes[NANVIX_PROC_MAX + 1];

	/* Initialization. */
	nodenum = sys_get_node_num();

	/* Retrieve kernel parameters. */
	assert(argc == 3);
	masternode = atoi(argv[1]);
	nclusters = atoi(argv[2]);

	/* Build nodes list. */
	nodes[0] = masternode;
	for (int i = 0; i < nclusters; i++)
		nodes[i + 1] = i;

	/* Synchronize with master. */
	assert((barrier = barrier_create(nodes, nclusters + 1)) >= 0);

	//==========================================================================
	//RTREE TESTE
  // if(argc!=2){
  //   printf("digite o tamanho do vetor\n");
  //   exit(1);
  // }

  //int n_rects = atoi(*(argv+1));
  //if(n_rects<=0){
  //  printf("INVALID INPUT\n" );
  //  exit(1);
  //}
  int n_rects = 10000;
  struct Rect *rects = malloc(sizeof(struct Rect) * n_rects);
  int i, n_search1,n_search3,n_search4,n_search5;
  double t_search1, t_search3,t_search4,t_search5;
  clock_t start, end;
  threads = 0;
  total_threads = 0;
  struct Node* root = RTreeNewIndex();

  //GERADOR DOS VETORES
  for(i=0;i<n_rects;i++){
    int j;
    for(j=0;j<NUMSIDES;j++){
      if(j<NUMDIMS)
      rects[i].boundary[j]=i;
      else
      rects[i].boundary[j]=i+1;
    }

  }

  //vetor usado para parar os threads
  kill = malloc(sizeof(struct Node));
  kill->level=-1;

  /*
  * Insert all the data rects.
  * Notes about the arguments:
  * parameter 1 is the rect being inserted,
  * parameter 2 is its ID. NOTE: *** ID MUST NEVER BE ZERO ***, hence the +1,
  * parameter 3 is the root of the tree. Note: its address is passed
  * because it can change as a result of this call, therefore no other parts
  * of this code should stash its address since it could change undernieth.
  * parameter 4 is always zero which means to add from the root.
  */
  for(i=0;i<n_rects;i++)
  RTreeInsertRect(&rects[i],i+1,&root,0);

  //rect a ser pesquisado
  struct Rect rect_search ;
  for(i=0;i<NUMSIDES/2;i++)
    rect_search.boundary[i]=0;

  for(i=NUMSIDES/2;i<NUMSIDES;i++)
    rect_search.boundary[i]=n_rects+1;

  //RTREESEARCH
  start = clock();
  n_search1 = RTreeSearch(root, &rect_search, MySearchCallback, 0);
  end = clock();
  t_search1 = (double)(end - start)/CLOCKS_PER_SEC;

  struct Search s_search1;
  s_search1.N=root;
  s_search1.R=&rect_search;
  s_search1.shcb=&MySearchCallback;
  s_search1.cbarg=NULL;
  n_search3=0;
  s_search1.hits = &n_search3;



  //RTREESEARCH3
  pthread_t thread_main;
  start = clock();
  pthread_create(&thread_main,NULL,RTreeSearch3,&s_search1);

  int teste_main;
  teste_main = pthread_join(thread_main,NULL);
  end = clock();
  t_search3 = (double)(end - start)/CLOCKS_PER_SEC;



  pthread_t thre[MAXTHR];
  struct Queue* q;
  QueueInit(&q);
  q->active= MAXTHR;
  struct Search s_search4[MAXTHR];
  int resp=0;
  for(i=0;i<MAXTHR;i++){
    s_search4[i].N=root;
    s_search4[i].R=&rect_search;
    s_search4[i].shcb=&MySearchCallback;
    s_search4[i].queue= &q;
    s_search4[i].cbarg=NULL;
    s_search4[i].hits=&n_search4;
    s_search4[i].flag=0;
    s_search4[i].depth = 0;
  }
  s_search4[0].flag=1;
  start= clock();
  for(i=0;i<MAXTHR;i++){
    pthread_create(&thre[i],NULL,RTreeSearch4,&s_search4[i]);
  }
  for(i=0;i<MAXTHR;i++)
  pthread_join(thre[i],NULL);
  end = clock();
  t_search4 = (double)(end - start)/CLOCKS_PER_SEC;


  //RTREESEARCH5
  Data s_search5[MAXTHR];
  n_search5=0;
  q->active= MAXTHR;
  q->inactive =0;
  for(i=0;i<MAXTHR;i++){
    s_search5[i].node=NULL;
    s_search5[i].rect = &rect_search;
    s_search5[i].queue = &q;
    s_search5[i].hits = &n_search5;
    pthread_create(&thre[i],NULL,InitThread,&s_search5[i]);
  }
  start=clock();
  QueuePush(&q,root);
  for(i=0;i<MAXTHR;i++){pthread_join(thre[i],NULL);}
  end = clock();
  t_search5 = (double)(end - start)/CLOCKS_PER_SEC;

  printf("RTREESEARCH: %d | RTREESEARCH3: %d| RTREESEARCH4: %d| RTREESEARCH5: %d\n",n_search1,n_search3,n_search4,n_search5);
  printf("RTREESEARCH: %.6lf | RTREESEARCH3: %.6lf| RTREESEARCH4: %.6lf| RTREESEARCH5: %.6lf\n", t_search1, t_search3,t_search4,t_search5 );
  //printf("%.6lf\n%.6lf\n%.6lf\n%.6lf\n\n", t_search1, t_search3,t_search4,t_search5 );
  QueueKill(&q);
  //==========================================================================
	printf("hello from node %d\n", nodenum);

	/* Synchronize with master. */
	assert(barrier_wait(barrier) == 0);

	/* House keeping. */
	assert(barrier_unlink(barrier) == 0);

	return (EXIT_SUCCESS);
}
