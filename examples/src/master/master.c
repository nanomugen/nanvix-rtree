/*
 * MIT License
 *
 * Copyright (c) 2011-2018 Pedro Henrique Penna <pedrohenriquepenna@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.  THE SOFTWARE IS PROVIDED
 * "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <mppaipc.h>

#include <nanvix/syscalls.h>
#include <nanvix/limits.h>
#include <nanvix/pm.h>

#include "index.h"
#include <stdio.h>

/**
 * @brief Number of processes.
 */
static int nclusters = 0;

/**
 * @brief Node number.
 */
static int nodenum = -1;

/*============================================================================*
 * Utilities                                                                  *
 *============================================================================*/

/**
 * @brief ID of slave processes.
 */
static int pids[NANVIX_PROC_MAX];

/**
 * @brief Barrier for global synchronization.
 */
static int barrier;

/**
 * @brief Spawns remote processes.
 */
static void spawn_remotes(void)
{
	char master_node[4];
	char nclusters_str[4];
	int nodes[nclusters + 1];
	const char *argv[] = {
		"/slave",
		master_node,
		nclusters_str,
		NULL
	};

	/* Build nodes list. */
	nodes[0] = nodenum;
	for (int i = 0; i < nclusters; i++)
		nodes[i + 1] = i;

	/* Create global barrier. */
	assert((barrier = barrier_create(nodes, nclusters + 1)) >= 0);

	/* Spawn remotes. */
	sprintf(master_node, "%d", nodenum);
	sprintf(nclusters_str, "%d", nclusters);
	for (int i = 0; i < nclusters; i++)
		assert((pids[i] = mppa_spawn(i, NULL, argv[0], argv, NULL)) != -1);
}

/**
 * @brief Wait for remote processes.
 */
static void join_remotes(void)
{
	/* Sync. */
	assert(barrier_wait(barrier) == 0);

	for (int i = 0; i < nclusters; i++)
		assert(mppa_waitpid(pids[i], NULL, 0) != -1);

	/* House keeping. */
	assert(barrier_unlink(barrier) == 0);
}

/*============================================================================*/

/**
 * @brief HAL RMem Microbenchmark Driver
 */


struct Rect rects[] = {
	// xmin, ymin, xmax, ymax (for 2 dimensional RTree)
<<<<<<< HEAD
	{{0, 0, 1, 1}},//0 
	{{1, 1, 2, 2}},//1
	{{2, 2, 3, 3}},//2
	{{3, 3, 4, 4}},//3
	{{4,4,5,5}},//4
	{{5,5,6,6}},//5
	{{6,6,7,7}},//6
	{{7,7,8,8}},//7
	{{8,8,9,9}},//8
	{{9,9,10,10}},//9
	{{10,10,11,11}},//10
	{{11,11,12,12}},//11
	{{12,12,13,13}},//12
	{{13,13,14,14}},//13
	{{14,14,15,15}},//14
	{{15,15,16,16}},//15
	{{16,16,17,17}},//16
	{{17,17,18,18}},//17
	{{19,19,20,20}},//18
	{{20,20,21,21}},//19
	{{21,21,22,22}},//20
	{{23,23,24,24}},//21
	{{24,24,25,25}},//22
	{{25,25,26,26}},//23
	{{7,5,7,5}},//24
=======
 {{0, 0, 1, 1}},//0 
 {{1, 1, 2, 2}},//1
 {{2, 2, 3, 3}},//2
 {{3, 3, 4, 4}},//3
 {{4,4,5,5}},//4
 {{5,5,6,6}},//5
 {{6,6,7,7}},//6
 {{7,7,8,8}},//7
 {{8,8,9,9}},//8
 {{9,9,10,10}},//9
 {{10,10,11,11}},//10
 {{11,11,12,12}},//11
 {{12,12,13,13}},//12
 {{13,13,14,14}},//13
 {{14,14,15,15}},//14
 {{15,15,16,16}},//15
 {{16,16,17,17}},//16
 {{17,17,18,18}},//17
 {{19,19,20,20}},//18
 {{20,20,21,21}},//19
 {{21,21,22,22}},//20
 {{23,23,24,24}},//21
 {{24,24,25,25}},//22
 {{25,25,26,26}},//23
 {{7,5,7,5}},//24
>>>>>>> f9f791ab2cfbffb42220bef30bd64ee693d5e5fb
};
int nrects = sizeof(rects) / sizeof(rects[0]);
struct Rect search_rect = {
	{6, 4, 10, 6}, // search will find above rects that this one overlaps
};
struct Rect search_rect2 = {
	{0, 1, 2, 1}, // search will find above rects that this one overlaps
};
struct Rect search_rect3 = {
	{9.1, 9.1, 9.1, 9.1}, // search will find above rects that this one overlaps
};

int MySearchCallback(int id, void* arg)
{
	// Note: -1 to make up for the +1 when data was inserted
        ((void) arg);
        printf("Hit data rect %d\n", id-1);
	return 1; // keep going
}
int main2(int argc, const char **argv)
{
	assert(argc == 2);

	//RTREE TESTE
	struct Node* root = RTreeNewIndex();
	int i, nhits,nhits2,nhits3;
	printf("nrects = %d\n", nrects);
	/*
	 * Insert all the data rects.
	 * Notes about the arguments:
	 * parameter 1 is the rect being inserted,
	 * parameter 2 is its ID. NOTE: *** ID MUST NEVER BE ZERO ***, hence the +1,
	 * parameter 3 is the root of the tree. Note: its address is passed
	 * because it can change as a result of this call, therefore no other parts
	 * of this code should stash its address since it could change undernieth.
	 * parameter 4 is always zero which means to add from the root.
	 */

	for(i=0; i<nrects; i++)
		RTreeInsertRect(&rects[i], i+1, &root, 0); // i+1 is rect ID. Note: root can change
	nhits = RTreeSearch(root, &search_rect, MySearchCallback, 0);
	printf("Search resulted in %d hits\n", nhits);
	RTreePrintNode(root,0);
	printf("MAXCARD: %d\n", MAXCARD);

	printf("SEGUNDA PESQUISA:\n");
	nhits2 = RTreeSearch(root, &search_rect2, MySearchCallback, 0);
	printf("Search 2 resulted in %d hits\n", nhits2);

	printf("\nTERCEIRA PESQUISA:\n");
	nhits3 = RTreeSearch(root, &search_rect3, MySearchCallback, 0);
	printf("Search 3 resulted in %d hits\n", nhits3);
	printf("\nsize of RTREE: %d\nsize of a node: %d\nsize of a rect: %d\n size of branch: %d\n",sizeof(*root),sizeof(struct Node), sizeof(struct Rect),sizeof(struct Branch));
<<<<<<< HEAD

	printf("FIM DO TESTE RTREE");
=======
	
	printf("FIM DO TESTE RTREE\n");
>>>>>>> f9f791ab2cfbffb42220bef30bd64ee693d5e5fb
	//RTREE TESTE FIM
	/* Retrieve kernel parameters. */
	nclusters = atoi(argv[1]);

	nodenum = sys_get_node_num();

	printf("hello from node %d\n", nodenum);

	spawn_remotes();
	join_remotes();

	return (EXIT_SUCCESS);
}
